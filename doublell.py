class Node:

    def __init__(self, data):

        self.prev = None
        self.next = None
        self.data = data

class DoubleLL:

    def __init__(self):

        self.head = None
        
    def countList(self):

        temp = self.head
        count = 0
        while (temp.next != None):
            temp = temp.next
            count = count + 1
        return count+1


    def insert(self, data, loc):
        new_node = Node(data)

        if self.head is None:
            new_node.prev = None
            self.head = new_node
        else:
            if loc == 'start':
                new_node.next = self.head
                new_node.prev = None

                if self.head is not None:
                    self.head.prev = new_node
                self.head = new_node
                return
            if loc=='end':
                cur = self.head
                while cur.next:
                    cur = cur.next
                
                cur.next = new_node
                new_node.prev = cur
                return
            else:
                temp = self.head
                count = self.countList()

                if (temp == None or count < loc):
                    print('index out of range')

                else:

                    i = 1
                    while(i < loc - 1):
                        temp = temp.next
                        i = i + 1

                    new_node.next = temp.next
                    (temp.next).prev = new_node

                    temp.next = new_node
                    new_node.prev = temp
                return


    def delete(self, loc):
        if self.head is None:
            print('list is empty')

        else:
            if loc == 'start' or loc==0:
                if self.head.next!=None:
                    self.head = self.head.next
                    self.head.prev = None
                else:
                    self.head = None
                return

            if loc == 'end' or loc== self.countList()-1:
                if self.head.next is None:
                    self.head = None
                else:
                    cur = self.head
                    previous = ''
                    while cur.next:
                        previous = cur
                        cur=cur.next
                    previous.next = None
                return
            else:
                temp = self.head
                count = self.countList()

                if count < loc:
                    print('index out of range')
                else:
                    i=1
                    p=None
                    cur=self.head
                    while i<loc:
                        p=cur
                        cur=cur.next
                        i+=1

                    t = cur.next
                    p.next = t
                    t.prev = p
                return


    def traverse(self):
        print('*'*20)
        cur = self.head
        while cur:
            print(cur.data)
            cur = cur.next
        print('*'*20)


    def reverse(self):
        temp = None
        cur = self.head
        while cur:
            temp = cur.prev
            cur.prev = cur.next
            cur.next = temp
            cur = cur.prev

        if temp:
            self.head = temp.prev

dlist = DoubleLL()


#insertion
dlist.insert(5,'start')
dlist.insert(4,'end')
dlist.insert(2,2)
dlist.traverse()

#reverse
dlist.reverse()
dlist.traverse()

#deletion
dlist.delete(2)
dlist.traverse()

dlist.delete('start')
dlist.traverse()

dlist.delete('end')
dlist.traverse()
